<?php

namespace App\GraphQL\Command;

use App\GraphQL\Helper\GraphqlSchemaBuildHelper;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'ox:graphql:generate-schema-builder',
    description: 'creates nameSpace cache for easy loading',
    aliases: ['ox:graphql:generate-schema']
)]
class SchemaBuilderCommand extends Command
{
    private string $projectDir;
    private GraphqlSchemaBuildHelper $schemaBuildHelper;
    private LoggerInterface $logger;

    /**
     * Initialize values
     *
     * @param KernelInterface          $kernel            KernelInterface object
     * @param GraphqlSchemaBuildHelper $schemaBuildHelper GraphqlSchemaBuildHelper object
     * @param LoggerInterface          $logger            LoggerInterface object
     */
    public function __construct(
        KernelInterface $kernel, GraphqlSchemaBuildHelper $schemaBuildHelper, LoggerInterface $logger
    ) {
        $this->projectDir = $kernel->getProjectDir();
        $this->schemaBuildHelper = $schemaBuildHelper;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     * Configure the command
     *
     * @return void
     */
    protected function configure(): void
    {
        $this ->setHelp('This command helps in configuring cache file...');
    }

    /**
     * Execute the command
     *
     * @param InputInterface  $input  InputInterface object
     * @param OutputInterface $output OutputInterface object
     *
     * @return int
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $schema = new Schema($this->schemaBuildHelper->generateSchema());
        } catch (InvariantViolation $e) {
            $this->logger->error($e->getMessage());
            throw new Error($e->getMessage());
        }
        $data = SchemaPrinter::doPrint($schema);
        file_put_contents($this->projectDir.'/config/schema.graphql', $data);
        return Command::SUCCESS;
    }

}
