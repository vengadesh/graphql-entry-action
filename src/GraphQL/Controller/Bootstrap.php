<?php
/**
 * This file is an Entry point of this project.
 * PHP version 8.1
 *
 * @category Graphql
 */

namespace App\GraphQL\Controller;

use App\GraphQL\Helper\GraphqlSchemaBuildHelper;
use ArrayAccess;
use Closure;
use Exception;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Validator\DocumentValidator;
use GraphQL\Validator\Rules\QueryDepth;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Entry point controller
 *
 * @category Graphql
 */
class Bootstrap extends AbstractController
{
    private string|int $debug;
    private mixed $schemaBuilder;
    private LoggerInterface $logger;

    /**
     * Initializing values
     *
     * @param GraphqlSchemaBuildHelper $schemaBuildHelper GraphqlSchemaBuildHelper object
     * @param LoggerInterface          $logger            LoggerInterface object
     */
    public function __construct(GraphqlSchemaBuildHelper $schemaBuildHelper, LoggerInterface $logger)
    {
        $this->debug = DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE;
        $this->schemaBuilder = $schemaBuildHelper;
        $this->logger = $logger;
    }

    /**
     * Initial validation of request
     *
     * @param Request $request request object
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Request $request): JsonResponse
    {
        $queryParameters = $request->get('graphqlQuery');
        $query = $queryParameters['query'] ?? null;
        $variables = $queryParameters['variables'] ?? [];
        $operationName = $queryParameters['operationName'] ?? null;
        if (null === $query) {
            $this->logger->error("Invalid Graphql Query.");
            throw new BadRequestHttpException(
                'Something went wrong. Please contact support team for further info.'
            );
        }
        DocumentValidator::addRule(new QueryDepth(10));
        $result = GraphQL::executeQuery(
            $this->schemaBuilder->getSchema(),
            $query,
            null,
            null,
            $variables,
            $operationName
        )
            ->toArray($this->debug);
        //The below formats the errors and shows only the message in prod env.
        if (!is_null($result['errors'] ?? null) && (strtolower($request->server->get('APP_ENV')) == 'prod')) {
            $result['errors'] = array_map(
                [$this, "reStructureError"], $result['errors']
            );
        }
        if (isset($result['errors'])) {
            $this->logger->error(json_encode($result['errors']));
            return new JsonResponse('Something went wrong. Please contact support team for further info.');
        }
        return new JsonResponse($result);
    }

    /**
     * Restructure error
     *
     * @param array $error error array
     *
     * @return null[]
     */
    #[ArrayShape(['message' => "mixed|null"])] private function reStructureError(array $error): array
    {
        return ['message' => $error['message'] ?? null];
    }
}
