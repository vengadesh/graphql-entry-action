<?php
/**
 * This file is an event subscriber
 * php version 8.1
 *
 * @category Graphql
 */
namespace App\GraphQL\EventSubscribers;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This class is an event for JWT token management
 *
 * @category Graphql
 */
class RequestValidateEvent implements EventSubscriberInterface
{

    private LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {

        $this->logger = $logger;
    }

    /**
     * Get subscribed events
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['validateRequestMethod', 100]
            ]
        ];
    }

    /**
     * Validate request method
     *
     * @param RequestEvent $requestEvent RequestEvent object
     *
     * @return void
     */
    public function validateRequestMethod(RequestEvent $requestEvent)
    {
        if ($requestEvent->getRequest()->getMethod() === Request::METHOD_OPTIONS) {
            $requestEvent->setResponse(new JsonResponse(['message' => "Method not allowed"], 201));
        }
        if ('json' !== $requestEvent->getRequest()->getContentType()) {
            $this->logger->error("Content type should be JSON.");
            throw new BadRequestHttpException(
                "Something went wrong. Please contact support team for further info."
            );
        }
        if (!in_array($requestEvent->getRequest()->getMethod(), ['POST', 'GET'])) {
            $this->logger->error("Request type should be GET (or) POST.");
            throw new BadRequestHttpException(
                "Something went wrong. Please contact support team for further info."
            );
        }
        try {
            $queryParameters = json_decode($requestEvent->getRequest()->getContent(), true);
        } catch (\ValueError $e) {
            $this->logger->error('Invalid Graphql Query. ' . $e->getMessage());
            throw new BadRequestHttpException(
                "Something went wrong. Please contact support team for further info."
            );
        }
        if (empty($queryParameters)) {
            $requestEvent->setResponse(
                new JsonResponse(['message' => "Empty body request.", 'status' => 204], 404)
            );
            return;
        }
        $requestEvent->getRequest()->attributes->set('graphqlQuery', $queryParameters);
    }

}
