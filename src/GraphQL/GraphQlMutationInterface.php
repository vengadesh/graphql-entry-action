<?php
/**
 * Interface that holds base method for mutation resolvers
 * PHP version 8.0
 *
 * @category Graphql
 */
namespace App\GraphQL;

use GraphQL\Type\Definition\ObjectType;

/**
 * Mutation interface
 *
 * @category Graphql
 */
interface GraphQlMutationInterface
{
    /**
     * Define Mutation
     *
     * @return  ObjectType
     */
    public function getMutation(): ObjectType;

}