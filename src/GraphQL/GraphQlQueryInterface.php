<?php
/**
 * Interface that holds base method for query resolvers
 * PHP version 8.0
 *
 * @category Graphql
 */
namespace App\GraphQL;

use GraphQL\Type\Definition\ObjectType;

/**
 * Query interface
 *
 * @category Graphql
 */
interface GraphQlQueryInterface
{
    /**
     * Define Query
     *
     * @return  ObjectType
     */
    public function getQuery(): ObjectType;

}