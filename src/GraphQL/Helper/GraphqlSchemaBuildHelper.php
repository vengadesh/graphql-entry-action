<?php
/**
 * Build graphql schema for the
 * PHP version 8.1
 *
 * @category Graphql
 */

namespace App\GraphQL\Helper;

use DirectoryIterator;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Schema;
use GraphQL\Utils\BuildSchema;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Graphql schema builder
 *
 * @category Graphql
 */
class GraphqlSchemaBuildHelper
{
    private string $projectDir;
    private string $queryInterfaceNamespace;
    private string $mutationInterfaceNamespace;
    private LoggerInterface $logger;

    /**
     * Initialize values
     *
     * @param KernelInterface $kernel kernel object
     * @param LoggerInterface $logger LoggerInterface object
     */
    public function __construct(KernelInterface $kernel, LoggerInterface $logger)
    {
        $this->projectDir = $kernel->getProjectDir();
        $this->queryInterfaceNamespace = 'App\GraphQL\GraphQlQueryInterface';
        $this->mutationInterfaceNamespace = 'App\GraphQL\GraphQlMutationInterface';
        $this->logger = $logger;
    }

    /**
     * Fetching schema
     *
     * @return Schema
     *
     * @throws Error
     */
    public function getSchema(): Schema
    {
        if (!file_exists($this->projectDir.'/config/schema.graphql')) {
            $this->logger->error("Schema.graphql file not found.");
            throw new Error("Something went wrong. Please contact support team for further info.");
        }
        try {
        $contents = file_get_contents($this->projectDir.'/config/schema.graphql');
        $typeConfigDecorator = function($typeConfig) {
            $name = $typeConfig['name'];
            if ($name === 'Query') {
                $typeConfig['resolveField'] =
                    function ($source, $args, $context, ResolveInfo $info) {
                    if ($info->fieldDefinition->name == 'login') {
                        if ($args['userName'] === 'test' && $args['password'] === '1234') {
                            return "Valid User.";
                        } else {
                            return "Invalid User";
                        }
                    } elseif ($info->fieldDefinition->name == 'validateUser') {
                        if ($args['age'] < 18) {
                            return ['userId' => $args['userId'], 'category' => 'Not eligible for voting'];
                        } else {
                            return ['userId' => $args['userId'], 'category' => 'Eligible for voting'];
                        }
                    } else {
                        return null;
                    }
                    };
            }
            return $typeConfig;
        };
            $schema = BuildSchema::build($contents, $typeConfigDecorator);
        } catch (Exception $e) {
            $this->logger->error("Error in schema.graphql file. " .$e->getMessage());
            throw new Error("Something went wrong. Please contact support team for further info.");
        }
        return $schema;
    }

    /**
     * Get Query types
     *
     * @return array
     * @throws Exception
     */
    public function getQueryTypes(): array
    {
        $queryType = [];
        foreach ($this->getNameSpaces('GET') as $nameSpace) {
                $object = new $nameSpace();
                $queryType [] = $object->getQuery();
        }
        return $queryType;
    }

    /**
     * Get Query types
     *
     * @return array
     *
     * @throws Exception
     */
    public function getMutationTypes(): array
    {
        $mutationType = [];
        foreach ($this->getNameSpaces('POST') as $nameSpace) {
            $object = new $nameSpace();
            $mutationType [] = $object->getMutation();
        }
        return $mutationType;
    }

    /**
     * Get schema classes
     *
     * @param string $interfaceName interface string
     *
     * @return string[]
     */
    public function fetchSchemaClasses(string $interfaceName): array
    {
        if (!interface_exists($interfaceName)) {
            throw new HttpException(
                500, "Interface missing, Please ensure the presence of ".
                substr($interfaceName, strrpos($interfaceName, trim("\ "))+1)
            );
        }
        return array_filter(
            $this->getFileNamesInsideDirectory($this->projectDir.'/src'),
            function ($className) use ($interfaceName) {
                if (class_exists($className) && in_array($interfaceName, class_implements($className))) {
                    return $className;
                }
                return null;
            }
        );
    }

    /**
     * Fetching schema
     *
     * @return array
     *
     * @throws Exception
     */
    public function generateSchema(): array
    {
        $schema = null;
        $mutation = $query = [];
        if (!is_null($this->getMutationTypes())) {
            foreach ($this->getMutationTypes() as $item) {
                $mutation += $item->config['fields'];
            }
            if (!empty($mutation)) {
                $schema['mutation'] = new ObjectType(['name' => 'Mutation', 'fields' => $mutation]);
            }
        }
        if (!is_null($this->getQueryTypes())) {
            foreach ($this->getQueryTypes() as $item) {
                $query += $item->config['fields'];
            }
            if (!empty($query)) {
                $schema['query'] = new ObjectType(['name' => 'Query', 'fields' => $query]);
            }
        }
        if (is_null($schema)) {
            $this->logger->error("Problem in building schema.");
            throw new UnprocessableEntityHttpException(
                "Something went wrong. Please contact support team for further info."
            );
        }
        return $schema;
    }

    /**
     * Load existingCache or build new
     *
     * @param string $method method name
     *
     * @return array
     * @throws Exception
     */
    public function getNameSpaces(string $method): array
    {
        return $this->loadData($method);
    }

    /**
     * Load data
     *
     * @param string $requestMethod request method
     * @param array  $namespaceData namespace data
     *
     * @return array
     * @throws Exception
     */
    public function loadData(string $requestMethod, array $namespaceData = []): array
    {
        if ($requestMethod === 'POST') {
            $namespaceData = $this->fetchSchemaClasses($this->mutationInterfaceNamespace);

        } elseif ($requestMethod === 'GET') {
            $namespaceData = $this->fetchSchemaClasses($this->queryInterfaceNamespace);
        }
        return $namespaceData;
    }

    /**
     * Get file names inside name spaces
     *
     * @param string $directoryPath directory path
     * @param array  $fileNameSpace namespace
     *
     * @return array
     */
    public function getFileNamesInsideDirectory(string $directoryPath, array $fileNameSpace = []): array
    {
        foreach (new DirectoryIterator($directoryPath) as $fileInfo) {
            if (!$fileInfo->isDot() ) {
                if ($fileInfo->isFile() && strtolower($fileInfo->getExtension()) == 'php') {
                    $fileNameSpace []= str_replace(
                        '/', '\\', str_replace(
                            $this->projectDir.'/src', 'App',
                            substr($fileInfo->getPathname(), 0, strlen($fileInfo->getPathname()) - 4)
                        )
                    );
                }
                if ($fileInfo->isDir()) {
                    $data = $this->getFileNamesInsideDirectory($fileInfo->getPathname());
                    if (is_array($data)) {
                        foreach ($data as $item) {
                            $fileNameSpace []= $item;
                        }
                    } else {
                        $fileNameSpace []= $data;
                    }
                }
            }
        }
        return $fileNameSpace ?? [];
    }
}