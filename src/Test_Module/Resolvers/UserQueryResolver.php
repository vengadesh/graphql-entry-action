<?php

namespace App\Test_Module\Resolvers;

use App\GraphQL\GraphQlQueryInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class UserQueryResolver implements GraphQlQueryInterface
{

    public function getQuery(): ObjectType
    {
        return new ObjectType(
            [
                'name' => 'userModuleQuery',
                'fields' => [
                    'login' => [
                        'args' => [
                            'userName'   => Type::nonNull(Type::string()),
                            'password'   => Type::nonNull(Type::string())
                        ],
                        'type' => Type::string()
                    ],

                    'validateUser' => [
                        'args' => [
                            'userId'   => Type::nonNull(Type::string()),
                            'age'   => Type::nonNull(Type::int())
                        ],
                        'type' => $this->validateUserReturnType()
                    ],
                    ]
                ]
        );
    }

    /**
     * Returns login credentials
     *
     * @return ObjectType
     */
    public function validateUserReturnType(): ObjectType
    {
        return $this->_returnType['validateUser'] ??= new ObjectType(
            [
                'name' => 'validateUser',
                'fields' =>[
                    'userId' => Type::string(),
                    'category' => Type::string(),
                ]
            ]
        );
    }
}