<?php

namespace App\Test_Module\Services;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class UserScalarReturnTypeService
{
    /**
     * Returns login credentials
     *
     * @return ObjectType
     */
    public function validateUserReturnType(): ObjectType
    {
        return $this->_returnType['validateUser'] ??= new ObjectType(
            [
                'name' => 'validateUser',
                'fields' =>[
                    'userId' => Type::int(),
                    'category' => Type::string(),
                ],
                'resolveField' => function ($data, $args, $context, ResolveInfo $info) {
                    return $data[$info->fieldName];
                }
            ]
        );

    }
}